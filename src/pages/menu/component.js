import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Appbar from "../../component/app-hotel";
import CardMenu from "../../component/card-menu";
import Cards from "../../component/card-promo";
import Data from "../../data/list-menu";
import Card from "@material-ui/core/Card";
import Gambar from "../../asset/-YHSwy6uqvk.png";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

export default function SimpleContainer(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xs" style={{ padding: 0 }}>
        <Typography component="div" style={{ height: "100vh" }}>
          <Appbar />

          <Grid
            item
            xs={12}
            style={{
              paddingTop: "15%",
              display: "flex",
              flexDirection: "column"
            }}
          >
            <img src={Gambar} style={{ margin: "40px 20px 0px 20px" }} alt="" />
            <Grid
              item
              xs={3}
              style={{
                backgroundColor: " #109666",
                borderRadius: "5px",
                marginTop: "-35px",
                marginLeft: 35,
                color: "white",
                paddingLeft: 15
              }}
            >
              <Typography>Promo</Typography>
            </Grid>
            <Typography className={classes.faforit}>
              Makanan Terfavorit
            </Typography>
            {Data.map(item => {
              if (item.jenis === "aneka ayam")
                return (
                  <CardMenu
                    nama={item.nama}
                    jenis={item.jenis}
                    harga={item.harga}
                    image={item.image}
                  />
                );
            })}
            {/* <Paper
                className={classes.button}
                style={{ backgroundColor: "white" }}
                variant="contained"
              >
                <Typography className={classes.promo}>promo</Typography>
              </Paper>
              <Typography className={classes.faforit}>
                Makanan Terfavorit
              </Typography> 
            {/* </Grid> */}
          </Grid>

          {/* {Data.map(item => {
            if (item.jenis === "aneka ayam")
              return (
                <CardMenu
                  nama={item.nama}
                  jenis={item.jenis}
                  harga={item.harga}
                  image={item.image}
                />
              );
          })} */}
        </Typography>
      </Container>
    </React.Fragment>
  );
}
