import Component from "./component";
import { withStyles } from "@material-ui/core/styles";
import style from "./styles";

const Styled = withStyles(style)(Component);

export default Styled;
