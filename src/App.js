import React from "react";
import Masuk from "./pages/masuk";
import Menu from "./pages/menu";
import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <Switch>
      <Route exact path="/">
        <Masuk />
      </Route>
      <Route path="/menu">
        <Menu />
      </Route>
    </Switch>
  );
}

export default App;
