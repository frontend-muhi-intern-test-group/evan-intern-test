import React from "react";
import UseStyles from "./styles";
import { withRouter } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import RoomIcon from "@material-ui/icons/Room";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import Gambar from "../../asset/back.png";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

function AppBarFood(props) {
  const classes = UseStyles();
  return (
    <AppBar
      justifyContent="center"
      position="static"
      className={classes.appbara}
      style={{
        backgroundColor: "#FBFBFB",
        position: "fixed",
        width: "100%",
        maxWidth: 448,
        boxShadow: "0px 4px 8px rgba(183, 178, 178, 0.51)",
        height: "79px"
      }}
    >
      <Toolbar>
        <ArrowBackIcon
          style={{ color: "#5E52AD", marginLeft: 5 }}
          className={classes.back}
          onClick={() => {
            props.history.push("/");
          }}
        />
        <Typography
          style={{
            color: "#000000",
            fontSize: 12,
            marginTop: -15,
            marginLeft: 9
          }}
        >
          Antar ke
        </Typography>
        <RoomIcon
          style={{
            color: "#F56363",
            fontSize: 15,
            marginLeft: -50,
            marginTop: 25
          }}
        />
        <Typography
          className={classes.hotel}
          style={{ color: "black", marginTop: 30, marginLeft: 7 }}
        >
          <strong>Hotel Dafam Semarang</strong>
        </Typography>
      </Toolbar>
    </AppBar>
  );
}
export default withRouter(AppBarFood);
