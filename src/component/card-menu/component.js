import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Image from "../../asset/Group 13.png";
import ButtonGroup from "@material-ui/core/ButtonGroup";

function Cardmenu(props) {
  const [number, setnumber] = useState(0);
  const { item } = props;
  const handlePlus = () => {
    setnumber(number + 1);
  };
  const handleTambah = () => {
    setnumber(number + 1);
  };
  const handleMinus = () => {
    setnumber(number - 1);
  };
  const hideButton = () => {
    if (number == 0) {
      return {
        backgroundColor: "#5E52AD"
      };
    }
    if (number !== 0) {
      return {
        display: "none"
      };
    }
  };

  const hideCount = () => {
    if (number == 0) {
      return {
        display: "none"
      };
    }
    if (number !== 0) {
      return {
        display: "flex"
      };
    }
  };
  const { nama, harga, jenis, image } = props;
  const { classes } = props;
  return (
    <React.Fragment>
      <CardContent>
        <CardMedia
          className={classes.grid}
          style={{ height: 100, width: 100, marginLeft: 15 }}
          image={image}
        />
        <Typography
          className={classes.nama}
          variant="h6"
          style={{ marginLeft: 140 }}
        >
          {nama}
        </Typography>
        <Typography
          className={classes.jenis}
          variant="body2"
          style={{ marginLeft: 140 }}
        >
          {jenis}
        </Typography>
        <Typography
          className={classes.hargi}
          variant="body2"
          style={{ marginLeft: 140 }}
        >
          {harga}
        </Typography>
      </CardContent>
      <Grid>
        <Button
          style={hideButton()}
          className={classes.tambah}
          onClick={() => handleTambah()}
          variant="contained"
          color="primary"
          disableElevation
        >
          <Typography style={{ marginTop: -7, fontSize: 14 }}>
            Tambah
          </Typography>
        </Button>
        <Grid style={hideCount()} container spacing={0}>
          <Grid item xs={4}></Grid>
          <Grid item xs={4}>
            <Button
              onClick={() => handleMinus()}
              className={classes.ples}
              style={{
                hideButton,

                paddingTop: 15
              }}
            >
              <Typography style={{ marginTop: -10 }}>-</Typography>
            </Button>
          </Grid>
          <Grid item xs={4}>
            <h3 className={classes.angka}>{number}</h3>
          </Grid>
          <Grid item xs={4}>
            <Button
              onClick={() => handlePlus()}
              className={classes.kurang}
              style={{
                boxShadow: " 0px 1px 4px rgba(109, 96, 96, 0.25)",
                height: "20px",
                width: "20px"
              }}
            >
              <Typography style={{ marginTop: -5 }}>+</Typography>
            </Button>
          </Grid>
          <Button>
            <img src={Image} className={classes.image} alt="" />
          </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
export default Cardmenu;
