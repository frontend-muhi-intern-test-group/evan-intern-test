const Styled = {
  grid: {
    marginLeft: -15,
    marginTop: 20,
    borderRadius: 15
  },
  nama: {
    marginLeft: 110,
    marginTop: -90,
    fontSize: 17,
    fontWeight: "bold"
  },
  card: {},
  jenis: {
    marginLeft: 110,
    color: "gray"
  },
  image: {
    marginTop: -180,
    marginLeft: 100,
    fontSize: 90
  },
  hargi: {
    fontSize: 18,
    marginLeft: 110,
    marginTop: 20
  },
  ples: {
    marginTop: -135,
    marginLeft: 120,
    fontSize: 25,
    paddingTop: -20,
    boxShadow: " 0px 1px 4px rgba(109, 96, 96, 0.25)",
    height: "10px",
    width: "20px"
  },

  kurang: {
    marginLeft: 310,
    marginTop: -180,
    fontSize: 25
  },
  angka: {
    marginTop: -65,
    marginLeft: 25
  },
  tambah: {
    backgroundColor: "#5E52AD",
    color: "white",
    marginLeft: 230,
    marginTop: -130,
    fontSize: 10,
    borderRadius: 5,
    height: 20
  }
};
export default Styled;
