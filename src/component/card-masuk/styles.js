const Styled = {
  root: {
    width: 350
  },
  gambar: {
    paddingTop: 50,
    height: 200,
    marginLeft: 55
  },
  selamat: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 24,
    color: "#5E52AD",
    paddingTop: 20
  },
  makanan: {
    fontSize: 13,
    color: "gray",
    paddingTop: 13,
    textAlign: "center"
  },
  button: {
    borderRadius: 25,
    width: 280,
    height: 40,
    backgroundColor: "#5E52AD",
    marginTop: "55px",
    marginLeft: 65
  },
  dengan: {
    color: "gray",
    fontSize: 12,
    paddingTop: 55
  },
  pengguna: {
    color: "gray",
    fontSize: 12,
    paddingTop: 5
  }
};
export default Styled;
