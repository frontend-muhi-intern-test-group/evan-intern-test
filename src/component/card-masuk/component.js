import React from "react";
import Gambar from "../../asset/Group 10.png";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";

function Masukpertama(props) {
  const { classes } = props;
  return (
    <React.Fragment
      position="static"
      justifyContent="center"
      className={classes.root}
    >
      <img src={Gambar} alt="" className={classes.gambar} />
      <Typography className={classes.selamat}>Selamat Datang</Typography>
      <Typography className={classes.makanan}>
        Masuk untuk menikmati maknan yang tersedia
      </Typography>
      <Button
        onClick={() => {
          props.history.push("/menu");
        }}
        className={classes.button}
        variant="contained"
        color="primary"
        disableElevation
      >
        Masuk Disini
      </Button>
      <Typography className={classes.dengan} style={{ textAlign: "center" }}>
        Dengan masuk dan mendaftar, Anda menyetujui <strong>Syarat</strong>
      </Typography>
      <Typography className={classes.pengguna} style={{ textAlign: "center" }}>
        Penggunaan dan Kebijakan Privasi
      </Typography>
    </React.Fragment>
  );
}
export default withRouter(Masukpertama);
